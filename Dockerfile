FROM registry.gitlab.com/simonkrenger/base-gin:latest
LABEL maintainer=simon@krenger.ch

WORKDIR /webroot

COPY css css/
COPY index.html .